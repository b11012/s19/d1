// alert("hi")

//Conditional statements allow us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise.

// if statement
// Executes a stement if a specified condition is true 

let numA = -1

/*
	Syntax:
	if(condition is true){
		statement/ code block
	};
*/

if(numA < 0){
	console.log('Hi, I am less than 0');
};

//The result of the expression added in the if's condition must result to true, else, the statement inside if() will not run.

//You can also check the condition. The expression results to a boolean true because of the use of the less than operator.
console.log(numA < 0)//results to true and so, the if statement was run.

numA = 0

//It will not run because the expression now results to false:
if(numA < 0){
	console.log("Hi I am from numA is 0");
};

console.log(numA < 0);
// result: false

// Another example:
let city = "New York"

if(city === "New York"){
	console.log("Welcome to New York City!");
};

// else if clause
/* 
    - Executes a statement if previous conditions are false and if the specified condition is true
    - The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

*/

let numH = 1;

if(numA < 0){
	console.log("Hello");

} else if(numH > 0){
	console.log("World");

};

//We were able to run the else if() statement after we evaluated that the if condition was failed.

//If the if() condition was passed and run, we will no longer evaluate to else if() and end the process there.

numA = 1

if(numA > 0){
	console.log("Hello");

} else if(numH > 0){
	console.log("World");
};

// Another example
city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City");

} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo!");
};
//Since we failed the condition for the first if(), we went to the else if() and checked and instead passed that condition.

// else statement
/* 
    - Executes a statement if all other conditions are false
    - The "else" statement is optional and can be added to capture any other result to change the flow of a program
*/
if(numA < 0){
	console.log("Hello");

} else if(numH === 0){
	console.log("World");

} else {
	console.log("Again");
};

//  else cannot run without if
/*else {
	console.log("Will not run without an if")
}*/
	// result: err

//  else if cannot run without if
/*else if (numH < 0){
	console.log("Will not run without an if")
}*/
	// result: err

// conditional statement inside a function

	/*
	    - Most of the times we would like to use if, else if and else statements with functions to control the flow of our application
	    - By including them inside functions, we can decide when certain conditions will be checked instead of executing statements when the JavaScript loads
	    - The "return" statement can be utilized with conditional statements in combination with functions to change values to be used for other features of our application
	*/

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){

	if(windSpeed < 30){
		return 'Not a typhoon yet';
	
	} else if(windSpeed <= 61){
		return 'Tropical depression detected';
	
	} else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected';
	
	} else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical storm detected';
	
	} else {
		return 'Typhoon detected'
	};
};

// Returns the string to the variable "message" that invoked it 
message = determineTyphoonIntensity(65);
console.log(message);


/* 
    - We can further control the flow of our program based on conditions and changing variables and results
    - The initial value of "message" was "No message."
    - Due to the conditional statements created in the function, we were able to reassign it's value and use it's new value to print a different output
    - console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code
*/
if(message == "Tropical storm detected"){
	console.warn(message);
};

// Truthy and Falsy
/* 
    - In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
    - Values are considered true unless defined otherwise
    - Falsy values/exceptions for truthy:
        1. false
        2. 0
        3. -0
        4. ""
        5. null
        6. undefined
        7. NaN
*/

// Truthy Examples
	/*
	- If the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
		
	*/

if(true){
	console.log("Boolean true is Truthy");
};

if(1){
	console.log("1 is Truthy");
};

if([]){
	console.log("Blank array is Truthy");
};

let isAdmin = true
isAdmin = 1

if(isAdmin === true){
	console.log("Admin is true")
};

if(isAdmin){
	console.log("Admin is true")
};

// Falsy Examples

if(false){
	console.log("This is Falsy");
};

if(0){
	console.log("0 is Falsy");
};

if(undefined){
	console.log("undefined is Falsy")
}

// Ternary Operators
	/*
		(expression) ? ifTrue : ifFalse;
	*/

// Single statement execution

let ternaryResult = (1 < 18) ? true : false
console.log("Result of Ternary Operator: " + ternaryResult); // result: true

//Multiple statement execution

let name;

function isOfLegalAge(){
	name = "Mark";
	return 'You are of legal age'
};

function isUnderAge(){
	name = "Michelle";
	return 'You are under the age limit'
};

let age = parseInt(prompt('What is your age?'));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log('Result of Ternary Operator in functions: ' + legalAge + ', ' + name)

// Switch Statement
	/*
		Syntax:
		switch (expression){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
	*/

let day = prompt("What day of the week is it today? "). toLowerCase();
console.log(day)

	switch(day){
		case 'monday' :
		console.log('Color of the day is red');
		break; 

		case 'tuesday' :
		console.log('Color of the day is blue');
		break;

		case 'wednesday' :
		console.log('Color of the day is yellow');
		break;

		case 'thursday' :
		console.log('Color of the day is green');
		break;

		case 'friday' :
		console.log('Color of the day is orange');
		break;

		case 'saturday' :
		console.log('Color of the day is pink');
		break;

		case 'sunday' :
		console.log('Color of the day is white');
		break;

		default:
		console.log('Please input a valid day');
		break;
	}

	// Try-Catch-Finally 

	function showIntensityAlert(windSpeed){

		try {
			alerat(determineTyphoonIntensity(windSpeed));

		} catch (error) {
			console.log(typeof error); //object
			console.warn(error.message);

		} finally {
			alert("Intensity updates will show new alert");
		}

	};

	showIntensityAlert(65);